#!/bin/bash
kubeadm reset
iptables -F
rm -rf /etc/cni
rm -rf /etc/kubernetes
rm -rf /var/lib/kubelet
systemctl restart kubelet
