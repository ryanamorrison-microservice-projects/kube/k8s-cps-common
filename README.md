k8s-cps-common
=========

Common tasks for readying control planes nodes (only). This role is designed to prepare a cluster of nodes (ec2, VM's, bare-metal, etc.) but it does not initialize the cluster. This series of roles is more purpose-specific than a general implementation like kube-spray.

Requirements
------------
Assumes certain ansible inventory groups are present in a specific hierarchy:
```
---
firstcp:
  hosts:
    cp1:
      var_host_endpoint_ip: 192.168.1.2
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
restcps:
  hosts:
    cp2:
      var_host_endpoint_ip: 192.168.1.3
      ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
allcps:
  children:
    firstcp:
    restcps:
  vars:
```
Also see variables below

Role Variables
--------------
Are broken into two categories based on where they are located.
### default variables
In defaults/main.yml there is a variable that controls whether testing scripts are installed on the cps: 
```
var_testing: false
```
### inventory variables
each node (ec2 instance, host, QEMU vm, etc.) is assumed to have a ssh key that CI runners will use to connect to the host:
```
ansible_ssh_private_key_file: "{{ lookup('env', 'HOME') }}/.ssh/{{ inventory_hostname }}"
```

Dependencies
------------
k8s-common      

Example Playbook
----------------
```
---
- name: Converge
  hosts: all
  gather_facts: true
  become: yes
  become_method: sudo
  tasks:
    - name: "include k8s-common role"
      ansible.builtin.include_role:
        name: k8s-cps-common
```
Or run `molecule test -s local-vagrant` (requires molecule, vagrant and virtualbox as configured)     

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)
